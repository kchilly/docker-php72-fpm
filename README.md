[![](https://images.microbadger.com/badges/version/kchilly/php72-fpm.svg)](https://microbadger.com/images/kchilly/php72-fpm "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/kchilly/php72-fpm.svg)](https://microbadger.com/images/kchilly/php72-fpm "Get your own image badge on microbadger.com")  
[![dockeri.co](https://dockeri.co/image/kchilly/php72-fpm)](https://hub.docker.com/r/kchilly/php72-fpm)

# docker-php72-fpm
This repository is the source of the `kchilly/php72-fpm` [docker image](https://hub.docker.com/r/kchilly/php72-fpm)

## Base
This image is based on `php:7.2-fpm` [official docker image](https://hub.docker.com/_/php).
